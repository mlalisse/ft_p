/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 02:05:06 by mlalisse          #+#    #+#             */
/*   Updated: 2014/05/18 09:29:35 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_p.h"

void	get_files(char *buf, int buf_size)
{
	DIR				*dp;
	struct dirent	*fp;

	dp = opendir("./");
	if (dp != NULL)
	{
		while ((fp = readdir(dp)) != NULL)
		{
			if (buf_size >= (int)ft_strlen(fp->d_name) + 1)
			{
				ft_strcpy(buf, fp->d_name);
				buf += ft_strlen(fp->d_name);
				buf_size -= ft_strlen(fp->d_name);
			}
			if (buf_size >= 1)
			{
				*(buf++) = ' ';
				buf_size--;
			}
		}
		closedir (dp);
	}
	*buf = '\0';
}

# include <errno.h>

void	handle_client(int s)
{
	size_t	len;
	char	*buf;
	char	lbuf[4096];

	buf = malloc(BUF_SIZE + 1);
	while ((len = recv(s, buf, BUF_SIZE, 0)) != 0)
	{
		buf[len] = '\0';
		if (ft_strcmp(buf, "pwd") == 0)
		{
			getcwd(lbuf, 4095);
			send(s, lbuf, ft_strlen(lbuf), 0);
		}
		else if (ft_strcmp(buf, "ls") == 0)
		{
			get_files(lbuf, 4095);
			send(s, lbuf, ft_strlen(lbuf), 0);
		}
		else if (ft_strncmp(buf, "cd ", 3) == 0)
			chdir(buf + 3);
		else if (ft_strncmp(buf, "get ", 4) == 0)
		{
			int		fd;
			if ((fd = open(buf + 4, O_RDONLY)) != -1)
			{
				while ((len = read(fd, lbuf, 4095)) > 0)
					send(s, lbuf, len, 0);
				close(fd);
				break ;
			}
		}
		else if (ft_strncmp(buf, "put ", 4) == 0)
		{
			int		fd;
			if ((fd = open(buf + 4, O_RDWR | O_CREAT | O_TRUNC, 0666)) != -1)
			{
				while ((len = recv(s, lbuf, 4095, 0)) > 0)
					write(fd, lbuf, len);
				close(fd);
				break ;
			}
		}
	}
	free(buf);
	close(s);
}

int		main(void) //int argc, char **argv)
{
	int					s;
	int					csd;
	socklen_t			addr_size;
	struct sockaddr_in	server_addr;
	struct addrinfo		*remote_addr;

	if ((s = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		printf("socket error\n");
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(8001);
	inet_pton(AF_INET, "0.0.0.0", &server_addr.sin_addr);
	if (bind(s, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
		ft_putendl("bind error");
	if (listen(s, 5) == -1)
		ft_putendl("listen error");
	addr_size = sizeof(remote_addr);
	while ((csd = accept(s, (struct sockaddr *)&remote_addr, &addr_size)) != -1)
	{
		if (csd == -1)
			ft_putendl("accept error");
		if (fork() == 0)
		{
			handle_client(csd);
			return (0);
		}
	}
	close(s);
	// freeaddrinfo(server_addr);
	return (0);
}
