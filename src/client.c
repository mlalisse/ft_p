/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 02:04:59 by mlalisse          #+#    #+#             */
/*   Updated: 2014/05/18 09:30:07 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_p.h"

int		main(void) //int argc, char **argv)
{
	struct sockaddr_in	address;
	char				lbuf[4096];
	char				*buf;
	int					s;

	if ((s = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		printf("socket error\n");
	// getaddrinfo("www.server.com", "http", &hints, &res); or gethostbyname
	address.sin_family = AF_INET;
	address.sin_port = htons(8001);
	inet_pton(AF_INET, "0.0.0.0", &address.sin_addr);
	if (connect(s, (struct sockaddr*)&address, sizeof(address)) != 0)
		printf("connect error\n");
	while (ft_putstr("> "),  get_next_line(0, &buf) != 0)
	{
		if (ft_strcmp(buf, "quit") == 0)
			break ;
		send(s, buf, ft_strlen(buf), 0);
		if (ft_strcmp(buf, "pwd") == 0)
		{
			lbuf[recv(s, lbuf, 4095, 0)] = '\0';
			printf("%s\n", lbuf);
		}
		else if (ft_strcmp(buf, "ls") == 0)
		{
			lbuf[recv(s, lbuf, 4095, 0)] = '\0';
			printf("%s\n", lbuf);
		}
		else if (ft_strncmp(buf, "put ", 4) == 0)
		{
			int		len, fd;
			if ((fd = open(buf + 4, O_RDONLY)) != -1)
			{
				while ((len = read(fd, lbuf, 4095)) > 0)
					send(s, lbuf, len, 0);
				close(fd);
				break ;
			}
		}
		else if (ft_strncmp(buf, "get ", 4) == 0)
		{
			int		len, fd;
			if ((fd = open(buf + 4, O_RDWR | O_CREAT | O_TRUNC, 0666)) != -1)
			{
				while ((len = recv(s, lbuf, 4095, 0)) > 0)
					write(fd, lbuf, len);
				close(fd);
				break ;
			}
		}
	}

	close(s);

	return (0);
}
