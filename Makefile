CFLAGS = -Wall -Wextra -L libft -lft -I libft/inc -I inc

all:
	gcc $(CFLAGS) -o serveur src/server.c
	gcc $(CFLAGS) -o client src/client.c
